package clases_basiques;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        int opcio = 0;

        do {
            System.out.println("Introduiex el numero de exercici que vols executar, del 1 al 12");
            System.out.println("0 = Sortir");
            opcio = llegirIntMinMax(0,12);
            switch (opcio) {
                case 0 -> {
                    break;
                }
                case 1 -> exercici1();
                case 2 -> exercici2();
                case 3 -> exercici3();
                case 4 -> exercici4();
                case 5 -> exercici5();
                case 6 -> exercici6();
                case 7 -> exercici7();
                case 8 -> exercici8();
                case 9 -> exercici9();
                case 10 -> exercici10();
                case 11 -> exercici11();
                case 12 -> exercici12();
                default -> System.out.println("Opció no valida");
            }

        }while (opcio != 0);



    }
    //1.- Llistar el nom de tots els fitxers d'un directori que li passem per teclat.
    public static void exercici1(){
        System.out.println("Introdueix el directori:");
        //String directori = "C:/Users/Usuario/Desktop/DAMi2/M03-09ProgramacioJava/dirPrueba";
        String directori = llegirStrTeclat();

        File path = new File(directori);

        if (path.exists() && path.isDirectory()) {
            //llistar contingut
            System.out.println("Llista: " + directori);
            File [] elements = path.listFiles();

            //RECORREMOS LOS ELEMENTOS
            for (File element: elements) {
                if (element.isFile()){
                    System.out.println("[FILE]"+element.getPath());
                } else {
                    System.out.println("[DIR]"+element.getName());
                }
            }
        } else {
            System.out.println("No existeix el path");
        }
    }

    //2.- Omplir un fitxer creat de nou amb els textos que li passes per teclat.
    public static void exercici2(){
        System.out.println("Introdueix el nom del fitxer:");
        String name_file = llegirStrTeclat();
        File file = new File(name_file);

        try {
            if (!file.exists()){
                file.createNewFile();
                FileWriter fw = new FileWriter(file);
                System.out.println("Quantes linies vols introduir? ");
                int num_linies = llegirIntMinMax(1,50);

                for (int i = 0; i < num_linies; i++) {
                    System.out.println("Linea " + i);
                    String textLine = llegirStrTeclat();
                    fw.write(textLine+"\n");
                }
                fw.close();
            } else {
                System.out.println("Ja existeix");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //3.- Afegir el que escrius per teclat al final d'un fitxer de text ja existent.
    public static void exercici3(){
        System.out.println("Introdueix la ruta absoluta del fitxer:");
        //String path = "/home/sjo/IdeaProjects/m6_acces_dades_/fitxer_de_proba";
        String path = llegirStrTeclat();
        File file = new File(path);
        System.out.println("Introdueix una linea, s'afegira al final del fitxer");
        String linea = llegirStrTeclat();
        if (file.exists() && file.isFile()){
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true)); //aqui el fallo?

                //FileWriter fw = new FileWriter(file, true);
                //fw.write(linea + "\n");

                bw.write(linea );
                bw.newLine();
                System.out.println("S'ha afegit correctament la linea");
                bw.close();

            } catch (Exception e){
                e.printStackTrace();
            }
        } else {
            System.out.println("EL fitxer no existeix");
        }
    }

    //4.- Llegir línia a línia el contingut d'un fitxer de text i mostrar-lo per pantalla.
    public static void exercici4(){
        try {
            File file = new File("fitxer_de_proba");
            Scanner lector = new Scanner(file);

            String linea = lector.nextLine();
            while (lector.hasNext()){
                System.out.println(linea);
                linea = lector.nextLine();
            }
            System.out.println(linea);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (Exception e) {
            System.out.println("Exception " + e);
        }
    }

    //5.- Llegir un fitxer línia a línia utilitzant la clase Scanner.
    public static void exercici5() throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introdueix la ruta completa del fitxer a llegir : ");
        String pathFile = llegirStrTeclat();
        File file = new File(pathFile);

        if (file.exists()) {
            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
            sc.close();
        } else {
            System.out.println("L'arxiu (" + file + ") no existeix!");
        }
    }

    //6.- Llegir el fitxer "enters.txt" que trobaràs a la carpeta "fitxers" al Classroom i que mostri
    // els números per pantalla, digui quants números hi i ha i mostri també la suma de tots els números.
    public static void exercici6() {
        File file = new File("enters.txt");
        int total_suma = 0;
        int total_num = 0;
        try {
            System.out.println("Numeros: ");
            Scanner lector = new Scanner(file);
            while (lector.hasNextInt()){
                int valor = lector.nextInt();
                System.out.print(valor+ ", ");
                total_suma +=valor;
                total_num++;
            }
            System.out.println();
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Hi han : " + total_num + " numeros.");
        System.out.println("La suma total del valors llegits es: " + total_suma);
    }

    //7.- Obtenir la línia més llarga i la més curta d'un fitxer de text.
    public static void exercici7() throws IOException {
        Scanner sc ;
        System.out.println("Introdueix la ruta y el nom del fitxer a llegir : ");
        String pathFile = llegirStrTeclat();
        File file = new File(pathFile);
        sc = new Scanner(file);
        if (file.exists()) {
            String linea, mayor = null, menor = null;
            linea = sc.nextLine();
            mayor = menor = linea;
            while (sc.hasNext()) {
                linea = sc.nextLine();
                if (linea.length() > mayor.length()) {
                    mayor = linea;
                } else if (linea.length() < menor.length()) {
                    menor = linea;
                }
            }
            System.out.println("Línia mes llarga : " + mayor);
            System.out.println("Línia mes curta : " + menor);
            sc.close();
        } else {
            System.out.println("L'arxiu (" + file + ") no existeix");
        }
    }

    //8.- Eliminar el fitxer que s'introdueixi la ruta pel teclat.
    public static void exercici8() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introdueix la ruta completa del fitxer a llegir : ");
        String pathFile = llegirStrTeclat();
        File file = new File(pathFile);
        if (file.delete()) {
            System.out.println("El fitxer se ha borrat correctament");
        } else {
            System.out.println("El fitxer no s'ha pogut eliminar");
        }
        sc.close();
    }

    //9.- Que per qualsevol ruta entrada per teclat mostri:
    //  ✔ Si el fitxer existeix o no
    //  ✔ Si es tracta d'un directori o d'un fitxer
    //  ✔ Si és un fitxer, ha de mostrar les següents dades:
    //      ➢ Nom
    //      ➢ Mida
    //      ➢ Permisos de lectura i escriptura
    public static void exercici9() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introdueix la ruta completa del fitxer a llegir : ");
        String pathFile = llegirStrTeclat();
        File file = new File(pathFile);

        if (file.exists()) {
            if (file.isFile()) {
                System.out.println("Nom del fitxer : " + file.getName());
                System.out.println("Tamany del fitxer : " + file.length());
                System.out.println("Permisos de Lectura : " + file.canRead());
                System.out.println("Permisos de Escritura : " + file.canWrite());
            } else if (file.isDirectory()) {
                System.out.println("És un directori");
            }
            sc.close();
        } else {
            System.out.println("L'arxiu/directori  (" + file + ") no existeix!");
        }
    }

    //10.Que llegeixi el fitxer "restaurants.csv" que trobaràs a la carpeta "fitxers" al
    // Classroom i mostri les dades de tots els restaurants que es trobin a l'Eixample.
    public static void exercici10() throws IOException {
        String fileCSV = "restaurants.csv";
        String linea = "";
        String separador = ",";
        BufferedReader br = new BufferedReader(new FileReader(fileCSV));

        while ((linea = br.readLine()) != null) {
            String[] campo = linea.split(separador);
            if (campo[3].equalsIgnoreCase("Eixample")) {
                for (int i = 0; i < 12; i++) {
                    System.out.print(campo[i] + separador);
                }
                System.out.println();
            }
        }
        br.close();
    }

    //11.- Que permeti afegir per teclat nous restaurants al fitxer "restaurants.csv"
    // utilitzant el mateix format que els que ja hi són (Nom,Telefon,Direccio,Districte,...).
    public static void exercici11() throws IOException {
        Scanner sc = new Scanner(System.in);
        String fileCSV = "restaurants.csv";
        File file = new File(fileCSV);

        if (file.exists() && file.isFile()) {
            FileWriter writer = new FileWriter(file, true);
            System.out.println("Nombre restaurant : ");
            String nom = llegirStrTeclat();
            System.out.println("Teléfon:");
            String telefon = llegirStrTeclat();
            System.out.println("Direcció:");
            String direccio = llegirStrTeclat();
            System.out.println("Districte:");
            String districte = llegirStrTeclat();
            System.out.println("Barrio:");
            String barri = llegirStrTeclat();
            System.out.println("Ciutat:");
            String ciutat = llegirStrTeclat();
            System.out.println("Códi Postal");
            String cp = llegirStrTeclat();
            System.out.println("Regió:");
            String regio = llegirStrTeclat();
            System.out.println("Pais:");
            String pais = llegirStrTeclat();
            System.out.println("Latitud:");
            String latitud = llegirStrTeclat();
            System.out.println("Longitut:");
            String longitud = llegirStrTeclat();
            System.out.println("Web:");
            String web = llegirStrTeclat();
            System.out.println("Email:");
            String email = llegirStrTeclat();
            writer.write(nom + "," + telefon + "," + direccio + "," + districte + "," + barri + "," + ciutat + "," + cp + "," + regio + "," + pais + "," + latitud + "," + longitud + "," + web + "," + email);
            sc.close();
            writer.close();
        } else {
            System.out.println("L'archiu (" + file + ") no existex");
        }
    }

    //12.- Que faci una còpia del fitxer "restaurants.csv" que es digui “restaurants2.csv”
    // que contingui les dades de tots els restaurants que no estiguin a l'Eixample.
    public static void exercici12() throws IOException {
        String fileCSV = "restaurants.csv";
        String linea = "";
        String separador = ",";
        BufferedReader br = new BufferedReader(new FileReader(fileCSV));
        FileWriter file = new FileWriter("restaurants2.csv");
        while ((linea = br.readLine()) != null) {
            String[] campo = linea.split(separador);
            if (!campo[3].equalsIgnoreCase("Eixample")) {
                file.write("\n");
                for (int i = 0; i < 12; i++) {
                    file.write(campo[i] + separador);
                }
            }
        }
        System.out.println("arxiu creat exitosament");
        br.close();

    }

    public static String llegirStrTeclat() {
        Scanner lector = new Scanner(System.in);
        String dades;
        dades = lector.nextLine();
        return dades;
    }

    public static int llegirIntTeclat() {
        Scanner lector = new Scanner(System.in);
        int enterLlegit = 0;
        boolean llegit = false;
        while (!llegit) {
            llegit = lector.hasNextInt();
            if (llegit) {
                enterLlegit = lector.nextInt();
            } else {
                System.out.println("ERROR DE TIPUS.");
                lector.next();
            }
        }
        lector.nextLine();
        return enterLlegit;
    }

    public static int llegirIntMinMax (int min, int max){
        int enterLlegit ;
        enterLlegit = llegirIntTeclat();
        while (enterLlegit < min || enterLlegit > max){
            System.out.println("ERROR DE RANG. MÍNIM: "+ min + ", MÀXIM: " + max);
            enterLlegit = llegirIntTeclat();
        }
        return enterLlegit;
    }


}
